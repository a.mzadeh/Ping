<?php

$host = 'www.asan-pm.com';

require_once 'DBUtils.php';
require_once 'JalaliDate.php';

$objDate = new jCalendar();
$logTime = $objDate->my_gregorian_to_jalali() . ' ' . date('H:i:s');
$isUp = false;

if($socket =@ fsockopen($host, 80, $errno, $errstr, 30)) {
   $isUp = true;
}

$db = new DBUtils();
$db->insert("insert into log (LogTime, IsUp) values(:LogTime, :IsUp)", array (
      ":LogTime" => $logTime,
      ":IsUp" => $isUp ? 1 : 0
   )
);

echo $isUp ? 'online!' : 'offline!';
