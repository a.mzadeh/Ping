<?php

class DBUtils {
	
	private $db = null;
	private $sth = null;
	
	public function __construct() {
	    $this->db = new PDO('sqlite:'.__DIR__.'/ping.sqlite');
	    $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

	public function execute($sql, $lstParams=null) {
		$this->sth = $this->db->prepare($sql);
		$this->sth->execute($lstParams);
	}

	public function insert($sql, $sqlParams=null) {
		$this->execute($sql, $sqlParams);
		return $this->db->lastInsertId();
	}

}

